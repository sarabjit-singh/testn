/*
 * @Author -- Sarabjit Singh
 */


package com.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LogInTest {
	
	WebDriver driver;
	
	@BeforeMethod
	public void Setup() {
		System.setProperty("webdriver.chrome.driver", "D://Automations Drivers//chromedriver//chromedriver.exe");	
		driver = new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.get("http://freecrm.com/");
	}
	
	
	@Test(priority=1)
	public void loginPageTitleTest(){
		WebDriverWait wait=new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[@class='brand-slogan'])[1]")));
		Assert.assertEquals(driver.getTitle(), "Free CRM software in the cloud");
	}
	
	@Test(priority=2)
	public void crmLogoImageTest(){
		boolean flag =driver.findElement(By.xpath("(//span[@class='brand-slogan'])[1]")).isDisplayed();
		Assert.assertTrue(flag);
	}
	
	
	@AfterMethod
	public void TearDown() {
		
		driver.quit();
		
	}

}
